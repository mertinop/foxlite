export const state = () => ({
  merchants: {},
  years: [],
  totalRevenues: {}
})

export const getters = {
  marketShare: state => (merchant, year) => {
    const revenues = state.merchants[merchant].revenues[year]
    const totalRevenues = state.totalRevenues[year]
    const getMarketShare = (sales, total) => sales / total * 100
    const marketShare = []
    for (let i = 0; i < revenues.length; i++) {
      marketShare.push(getMarketShare(revenues[i], totalRevenues[i]).toFixed(2))
    }
    return marketShare
  }
}

export const mutations = {
  INIT_MERCHANTS (state, merchants) {
    for (const merchantName of merchants) {
      state.merchants[merchantName] = {
        name: merchantName,
        revenues: {}
      }
    }
  },
  SET_TOTAL_REVENUES (state, totalRevenues) {
    const { year, values } = totalRevenues
    state.totalRevenues[year] = []
    for (const revenue of values) {
      const { value } = revenue
      state.totalRevenues[year].push(value)
    }
  },
  SET_MERCHANT_REVENUES (state, data) {
    const { year, merchant, values } = data
    state.merchants[merchant].revenues[year] = []
    const revenues = values.map(v => v.value)
    state.merchants[merchant].revenues[year] = revenues
  }
}

export const actions = {
  async GET_MERCHANTS ({ commit, state }) {
    if (Object.values(state.merchants).length === 0) {
      const merchants = await this.$axios.$get('merchants')
      commit('INIT_MERCHANTS', merchants)
    }
  },
  async GET_REVENUES ({ dispatch, commit, state }, payload) {
    const { merchant, year } = payload
    if (!state.merchants[merchant]) {
      await dispatch('GET_MERCHANTS')
    }
    if (!state.totalRevenues[year]) {
      const totalRevenues = await this.$axios.$get(`revenue/${year}`)
      commit('SET_TOTAL_REVENUES', totalRevenues)
    }
    if (!state.merchants[merchant].revenues[year]) {
      const merchantShare = await this.$axios.$get(`revenue/${year}/${merchant}`)
      commit('SET_MERCHANT_REVENUES', merchantShare)
    }
  }
}
