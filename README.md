# foxlite-nuxt
Hiring test Foxintelligence

Martin Santibanez, September 2019.

Live: [https://foxlite-msa.herokuapp.com/](https://foxlite-msa.herokuapp.com/) (It might take some seconds to load as I have the free version of Heroku)

## Considerations
* Server rendering: I used the test as an excuse to learn how to use [Nuxt](https://nuxtjs.org/). Other than providing an incredibly easy interface to develop on Vue, it's used to overcome the CORS restriction by using the [proxy module](https://github.com/nuxt-community/proxy-module). It's nice because we don't display the real requests, and the client sees that they're done to localhost.
* Cache: As it's expensive to retrieve data and data won't change on the short time, API calls are done only once per session, and then stored on the state. This happens on the `GET_REVENUES` action (file `store/index.js`)
* For the chart I used the library [vue-chartjs](https://vue-chartjs.org/) as I have already used it before. Depending on the requirements something more or less complex could be used. 
* During The last year I've worked with ReactJS only, so maybe some stuff might not be best practices on Vue. I spent some time remembering how to use Vue and Vuex properly.
* I tried to keep the project simple, on a bigger one for example I would have defined types for the state actions.
* Didn't focus on the design, other than using the default styles that came with nuxt.
* For simplicity, data is only for the year 2018 as stated on the test, but this could (and maybe should) be more general and I would provide a way to select a different year.

## Assumptions
* Data will come in order by month.


## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Final word

Thank you for your time! I enjoyed doing the test, and learned some stuff.